# INSTRUCTIONS #

- This test is based on a page layouting using Bootstrap as a Framework with two preprocessors

- The work given by the designer is located in ./design/

- The images that must be used are located in ./design/img/

- Use Font Awesome as icons resource

- The typographies that must be used are:
    - Hind
    - Roboto
    * *They are located in https://www.google.com/fonts*

- This test uses two preprocessors:
    - Jade for HTML
    - SASS for CSS

- Before starting this test, read the README.md file

- After finishing this test, the work done must be pushed to this repository using GIT

## Important ###

All the generated code must be located in the **./app** folder. For the *CSS* generation just use **./app/styles/main.scss** and for the *HTML* generation you can use **./app/views/index.jade**.

## Useful tools ##
- http://html2jade.org/
- http://css2sass.herokuapp.com/
- Our email: support@bazzite.com

