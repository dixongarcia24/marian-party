'use strict';
/*global alertas*/
$(document).ready(function(){
  $('#nuevo').prop('disabled', true);
  $('#viejo').prop('disabled', true);
  $('#servicios').prop('disabled', true);
  $('#estados').prop('disabled', true);
  var urlServicios = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=10';
  $.getJSON(urlServicios,function(data){
    if(data.existe === 'true'){
      $.each(data.servicios,function(){
        $.each(this,function(i,item){
          $('#servicios').append('<option value="'+i+'">'+item+'</option>');
        });
      });
      $('#servicios').prop('disabled', false);
    }
  });
  var urlEstados = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=4';
  $.getJSON(urlEstados,function(data){
    if (data.existe === 'true'){
      $.each(data.estados,function(){
        $.each(this,function(i,item){
          $('#estados').append('<option value="'+i+'">'+item+'</option>');
        });
      });
      $('#estados').prop('disabled', false);
    }
  });
  $('#ciudad').prop('disabled', true);
  $('#nuevoTrans').prop('disabled', true);
  $('#servicios').change(function(){
    var servicio = $(this).val();
    $('#nuevo').prop('disabled', true);
    $('#nuevo').val('');
    $('#viejo').val('');
    $('#viejo').val('Por favor espere...');
    if (servicio>0){
      var urlPrecio = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=11&id='+servicio;
      $.getJSON(urlPrecio,function(data){
        if(data.existe==='true'){
          $('#viejo').val('');
          $('#viejo').val(data.precio);
          $('#nuevo').prop('disabled', false);
        }
      });
    }else{
      $('#viejo').val('');
    }
  });

  $('#enviarPrecio').click(function(){
    var precioNuevo = $('#nuevo').val();
    var servicio = $('#servicios').val();
    if (precioNuevo === ''){
      alertas('alert-danger','Debe ingresar los datos solicitados.','fa-exclamation-triangle');
      $('#nuevo').focus();
    }else{
      var urlNuevoPrecio = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=15&id='+servicio+'&precio='+precioNuevo;
      $.getJSON(urlNuevoPrecio,function(data){
        if(data.existe === 'true'){
          alertas('alert-success','Se ha actualizado el precio del servicio correctamente.','fa-check');
        }else{
          alertas('alert-danger','Ocurrió un error, intente más tarde.','fa-exclamation-triangle');
        }
        $('#viejo').val('');
        $('#nuevo').prop('disabled', true);
        $('#nuevo').val('');
        $('#servicios').prop('selectedIndex', 0);
      });
    }
  });

  $('#btnSuscriptores').click(function(){
    $('#loading').show();
    var cantidad = $('#cantidad').val();
    var separador = $('input[name=radioSeparador]:checked').val();
    if (cantidad === '' || separador === 'undefined'){
      alertas('alert-danger','Debe ingresar los datos solicitados.','fa-exclamation-triangle');
      $('#cantidad').focus();
      $('#loading').hide();
    }else{
      var urlSuscriptores = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=9';
      $.getJSON(urlSuscriptores,function(data){
        var texto = '';
        var i = 1;
        console.log(cantidad);
        if (data.existe==='true') {
          texto = '<p>';
          $.each(data.correos,function(item){
            if (i > cantidad){
              texto = texto +'<p><hr/>';
            }
            if (i===parseInt(cantidad)){
              texto = texto + data.correos[item];
              texto = texto +'</p>';
              i = 1;
            }else{
              texto = texto + data.correos[item] + separador + ' ';
            }
            i++;
          });
          if(i<cantidad){
            texto = texto + '</p>';
          }
          $('#suscriptoresResultado').empty();
          $('#suscriptoresResultado').append(texto);

        }
      });
      $('#loading').hide();
    }
  });

  $('#estados').change(function(){
    $('#ciudad').prop('disabled', true);
    var estado = $(this).val();
    $('#ciudad').empty();
    $('#ciudad').append('<option>Por favor espere...</option>');
    if (estado>0){
      var urlCiudad = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=5&estado='+estado;
      $.getJSON(urlCiudad,function(data){
        if(data.existe==='true'){
          $('#ciudad').empty();
          $.each(data.ciudades,function(){
            $.each(this,function(i,item){
              $('#ciudad').append('<option value="'+i+'">'+item+'</option>');
              $('#ciudad').prop('disabled', false);
            });
          });
        }
      });
    }else{
      $('#ciudad').empty();
      $('#ciudad').append('<option>Seleccione</option>');
      $('#ciudad').prop('disabled', true);
    }
  });

  $('#ciudad').change(function(){
    var ciudad = $(this).val();
    $('#nuevoTrans').prop('disabled', true);
    $('#viejoTrans').val('');
    $('#viejoTrans').val('Por favor espere...');
    if (ciudad >0){
      var urlPrecioCiudad = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=16&id='+ciudad;
      $.getJSON(urlPrecioCiudad,function(data){
        if(data.existe === 'true'){
          $('#nuevoTrans').prop('disabled', false);
          $('#viejoTrans').val(data.precio);
        }
      });
    }
  });

  $('#enviarPrecioTrans').click(function(){
    $('#loading').show();
    var precioNuevo = $('#nuevoTrans').val();
    var ciudad = $('#ciudad').val();
    if (precioNuevo === ''){
      alertas('alert-danger','Debe ingresar los datos solicitados.','fa-exclamation-triangle');
      $('#nuevoTrans').focus();
    }else{
      var urlNuevoPrecio = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=17&id='+ciudad+'&precio='+precioNuevo;
      $.getJSON(urlNuevoPrecio,function(data){
        if(data.existe === 'true'){
          alertas('alert-success','Se ha actualizado el precio de transporte correctamente.','fa-check');
        }else{
          alertas('alert-danger','Ocurrió un error, intente más tarde.','fa-exclamation-triangle');
        }
        $('#loading').hide();
        $('#estados').prop('selectedIndex', 0);
        $('#nuevoTrans').val('');
        $('#viejoTrans').val('');
        $('#ciudad').empty();
        $('#ciudad').append('<option>Seleccione</option>');
        $('#ciudad').prop('disabled', true);
      });
    }
  });

});
