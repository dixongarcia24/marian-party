/* jshint devel:true */
/*global alertas, resetError, mostrarSesion, showLoader, hideLoader, errorLoader*/
'use strict';

$('.dropdown').on('show.bs.dropdown', function(e){ // jshint ignore:line
  $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  $(this).find('.fa').removeClass('fa-caret-down');
  $(this).find('.fa').addClass('fa-caret-up');
});

// ADD SLIDEUP ANIMATION TO DROPDOWN //
$('.dropdown').on('hide.bs.dropdown', function(e){ // jshint ignore:line
  $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  $(this).find('.fa').removeClass('fa-caret-up');
  $(this).find('.fa').addClass('fa-caret-down');
});


$(document).ready(function(){
  if(sessionStorage.statusMP === 'true'){
    var username = 'Hola, '+ sessionStorage.abrUsuarioMP +' '+ sessionStorage.nombreUsuarioMP +', a continuación aparecen los servicios que desea contratar.';
    $('#seleccionUsuario').append(username);
    var urlEstados = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=4';
    $.getJSON(urlEstados,function(data){
      if (data.existe === 'true'){
        $.each(data.estados,function(){
          $.each(this,function(i,item){
            $('#estados').append('<option value="'+i+'">'+item+'</option>');
          });
        });
      }
    });
  }else{
    $('#myModal').modal({'backdrop':'static'});
  }
  $('#closeModalSign').click(function(){
    window.location = '/';
  });
  $('.datepicker').datepicker({
    startDate: 'd',
    maxViewMode: 1,
    language: 'es',
    daysOfWeekHighlighted: '0,6',
    autoclose: true
  });

  $('#signIn').click(function(){
    resetError('#errorLogin');
    var email = $('#emailLogin').val();
    var password = $('#passwordLogin').val();
    if(email ==='' || password ===''){
      $('#errorLogin').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Email o contraseña no puede ser vacío.');
      $('#myModal').shake(3,15,800);
      $('#emailLogin').focus();

    }
    else{
      showLoader('#myModal');
      var url='//beta.marianparty.com/MPWebService/despachador.php?servicio=1&email='+email+'&clave='+password;
      $.getJSON(url, function(data){
        if(data.existe === 'true'){
          if(data.status === '0'){
            errorLoader('#myModal');
            $('#errorLogin').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> El usuario no ha sido confirmado, por favor verifique su correo y confirme su cuenta.');
            $('#myModal').shake(3,15,800);
          }else{
            var username = 'Hola, '+ data.abr +' '+ data.nombre +', a continuación aparecen los servicios que desea contratar.';
            $('#seleccionUsuario').append(username);
            sessionStorage.idUsuarioMP = data.id;
            sessionStorage.abrUsuarioMP = data.abr;
            sessionStorage.admin = data.admin;
            sessionStorage.nombreUsuarioMP = data.nombre;
            sessionStorage.emailUsuarioMP = data.email;
            sessionStorage.statusMP = 'true';
            var urlEstados = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=4';
            $.getJSON(urlEstados,function(data){
              if (data.existe === 'true'){
                $.each(data.estados,function(){
                  $.each(this,function(i,item){
                    $('#estados').append('<option value="'+i+'">'+item+'</option>');
                  });
                });
              }
            });
            alertas('alert-success','Bienvenido.','fa-check');
            setTimeout(function(){
              $('.alert').alert('close');
            },5000);
            hideLoader('#myModal');
            mostrarSesion();
          }
        }else{
          errorLoader('#myModal');
          $('#errorLogin').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Invalida combinación email/contraseña');
          $('input[type="password"]').val('');
          $('#myModal').shake(3,15,800);
        }
      });
    }
  });

  $('#signUp').click(function(){
    resetError('#errorRegistration');
    var abr = $('#abrName').val();
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $('#mobile').val();
    var company = $('#company').val();
    var password = $('#password').val();
    var ciudad = $('#city').val();
    var reCaptchaResponse = $('#g-recaptcha-response').val();
    if (name ==='' || email ==='' || phone === '' || password === ''){
      $('#errorRegistration').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Debe completar todos los datos para poder registrarse.');
      $('#myModal').shake(3,15,800);
    }else{
      if (reCaptchaResponse){
        showLoader('#myModal');
        var urlCaptcha = '//beta.marianparty.com/MPWebService/despachador.php?servicio=3&response='+reCaptchaResponse;
        $.getJSON(urlCaptcha,function(data){
          if(data.success === false){
            errorLoader('#myModal');
            $('#errorRegistration').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Eres un Bot sal de aquí!!');
            $('#myModal').shake(3,15,800);

          }else{
            var urlRegistro = '//beta.marianparty.com/MPWebService/despachador.php?servicio=2&abr='+abr+
            '&nombre='+name+'&email='+email+'&telefono='+phone+'&empresa='+company+
            '&ciudad='+ciudad+'&clave='+password;
            $.getJSON(urlRegistro,function(data){
              if(data.existe === 'true'){
                errorLoader('#myModal');
                $('#errorRegistration').addClass('alert alert-success').html('<i class="fa fa-check"> Bienvenido, le hemos enviado un correo para la confirmación de su cuenta.');
              }else if (data.existe==='no-mail') {
                hideLoader('#myModal');
                alert('Hemos tenido un problema al enviar el correo de confirmación, intente más tarde');
                window.location='/';
              }else if(data.existe==='registrado'){
                errorLoader('#myModal');
                $('#errorRegistration').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> El correo introducido ya está asociado a un usuario.');
                $('#myModal').shake(3,15,800);
              }else{
                errorLoader('#myModal');
                $('#errorRegistration').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Ocurrió un error al registrarse, intente más tarde.');
                $('#myModal').shake(3,15,800);
              }
            });
          }
        });
      }else{
        errorLoader('#myModal');
        $('#errorRegistration').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Por favor selecciona el captcha.');
        $('#myModal').shake(3,15,800);
      }
    }

  });

  $('#estados').change(function(){
    $('#ciudad').prop('disabled', true);
    var estado = $(this).val();
    $('#ciudad').empty();
    $('#ciudad').append('<option>Por favor espere...</option>');
    if (estado>0){
      var urlCiudad = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=5&estado='+estado;
      $.getJSON(urlCiudad,function(data){
        if(data.existe==='true'){
          $('#ciudad').empty();
          $.each(data.ciudades,function(){
            $.each(this,function(i,item){
              $('#ciudad').append('<option value="'+i+'">'+item+'</option>');
              $('#ciudad').prop('disabled', false);
            });
          });
        }
      });
    }else{
      $('#ciudad').empty();
      $('#ciudad').append('<option>Seleccione</option>');
      $('#ciudad').prop('disabled', true);
    }
  });

  $('#confirmarCot').click(function(){
    var fecha = $('#fechaEvento').val();
    var ciudad = $('#ciudad').val();
    var direccion = $('#direccion').val();
    if (fecha === '' || ciudad > 0 || direccion === '' ){
      $('#errorCotizar').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Debe completar todos los datos para poder cotizar.');
      $('#modalCotizar').shake(3,15,800);
    }else{
      showLoader('#modalCotizar');
      var arregloSeleccion = [];
      $('#checkable-output li p').each(function(){
        arregloSeleccion.push($(this).attr('id'));
      });
      arregloSeleccion = JSON.stringify(arregloSeleccion);
      var id = sessionStorage.getItem('idUsuarioMP');
      var url = 'http://beta.marianparty.com/MPWebService/despachador.php?servicio=14&id='+id+'&fecha='+fecha+'&seleccion='+arregloSeleccion+'&ciudad='+ciudad+'&direccion='+direccion;
      $.getJSON(url,function(data){
        if (data.existe ==='true'){
          alertas('alert-success','La cotización ha sido enviada exitosamente a su correo electrónico.','fa-check');
        }else{
          alertas('alert-danger','Ha ocurrido un problema, codigo de error: '+data.existe+' .','fa-exclamation-triangle');
        }
      });
      hideLoader('#modalCotizar');
    }
  });

});
