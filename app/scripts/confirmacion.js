'use strict';

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function() {
  $('#loading').show();
  var usuario = getUrlParameter('idusuario');
  var token = getUrlParameter('token');
  var url = '//beta.marianparty.com/MPWebService/despachador.php?servicio=6&idusuario='+usuario+'&token='+token;
  $.getJSON(url,function(data){
    if (data.existe === 'true'){
      $('#mensaje').empty();
      $('#mensaje').append('Gracias por confirmar su cuenta.<br/> Ahora puede disfrutar de todos los servicios de <stron>MarianParty.com</strong>');
      $('#mensaje').after('<a href="/"><button class="btn btn-primary">Ir al Inicio</button>');
      $('#imagen').attr('src','/images/utils/confirmacion_jump.jpg');
      $('#loading').hide();
    }
    else if (data.existe==='false') {
      $('#mensaje').empty();
      $('#mensaje').append('Disculpe, el enlace está caducado o ya fue usado<br/> Le invitamos a restablecer su contraseña o a registrarse, si es el caso.');
      $('#mensaje').after('<a href="/"><button class="btn btn-primary">Ir al Inicio</button>');
      $('#imagen').attr('src','/images/utils/confirmacion_mini.jpg');
      $('#loading').hide();
    }else if (data.existe==='NoActivate') {
      $('#mensaje').empty();
      $('#mensaje').append('Disculpe, no hemos podido activar su cuenta<br/> Lamentamos que esto haya sucedido, si este error persiste envía un correo a <a href="mailto:info@marianparty.com">info@marianparty.com</a>');
      $('#mensaje').after('<a href="/"><button class="btn btn-primary">Ir al Inicio</button>');
      $('#imagen').attr('src','/images/utils/confirmacion_mini.jpg');
      $('#loading').hide();
    }
  });
});
