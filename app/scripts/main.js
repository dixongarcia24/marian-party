/* jshint devel:true */
/*jshint unused:false*/
/*global alertas, shake, mostrarSesion, resetError, showLoader, hideLoader, errorLoader, mostrarSesion */

'use strict';

function mostrarSesion(){
  $('#user').show();
  $('#nameUser').empty();
  $('#nameUser').append('<i class="fa fa-user"></i> '+sessionStorage.abrUsuarioMP +' '+ sessionStorage.nombreUsuarioMP);
  $('#adminPanel').empty();
  $('#adminPanel').attr('href','');
  if(sessionStorage.admin === '1'){
    $('#adminPanel').append('<i class="fa fa-tachometer"></i>');
    $('#adminPanel').attr('href','/panel.html');
  }
}

if(sessionStorage.statusMP === 'true'){
  mostrarSesion();
}else{
  $('#user').hide();
  $('#nameUser').empty();
  $('#adminPanel').empty();
  $('#adminPanel').attr('href','');
}

$('#closeUser').click(function(){
  if(confirm('¿Deseas cerrar su sesión?')){
    sessionStorage.clear();
    $('#user').hide();
    var x = window.location.pathname;
    if(x==='/cotizar.html' || x==='/panel.html' || x==='/panel' || x==='/cotizar'){
      window.location = '/';
    }
  }
});

$('#myModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var title = button.data('title'); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.modal-title').text(title);
  //modal.find('.modal-body input').val(recipient)
});


jQuery.fn.shake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        for (var x=1; x<=intShakes; x++) {
        $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
    .animate({left:intDistance}, ((intDuration/intShakes)/2))
    .animate({left:0}, (((intDuration/intShakes)/4)));
    }
  });
return this;
};

function alertas(clase,msj,icon){
  var div = '<div class="container" style="padding: 0 15%;">'+
            '<div class="alert '+clase+' fade in">'+
            '<a href="#" class="close" data-dismiss="alert">&times;</a>'+
            '<strong><i class="fa '+icon+'"></i> </strong>'+msj+'</div></div>';
  $('.wrap').after(div);
  setTimeout(function(){
    $('.alert').alert('close');
  },5000);
}


function resetError(id){
  $(id).removeClass('alert alert-danger').html('');
}

function showLoader(modal){
  $('#loading').show();
  $(modal).hide();
}
function hideLoader(modal){
  $('#loading').hide();
  $(modal).modal('toggle');
}
function errorLoader(modal){
  $('#loading').hide();
  $(modal).show();
}

$('#signInFoo').click(function(){
  resetError('#errorLoginFoo');
    var email = $('#emailLoginFoo').val();
    var password = $('#passwordLoginFoo').val();
    if(email ==='' || password ===''){
      $('#errorLoginFoo').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Email o contraseña no puede ser vacío.');
      $('#modalSign').shake(3,15,800);
      $('#emailLoginFoo').focus();
    }else{
      showLoader('#modalSign');
    var url='//beta.marianparty.com/MPWebService/despachador.php?servicio=1&email='+email+'&clave='+password;
      $.getJSON(url, function(data){
        if(data.existe === 'true'){
          if(data.status === '0'){
            errorLoader('#modalSign');
            $('#errorLoginFoo').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> El usuario no ha sido confirmado, por favor verifique su correo y confirme su cuenta.');
            $('#modalSign').shake(3,15,800);
          }else{
            sessionStorage.idUsuarioMP = data.id;
            sessionStorage.abrUsuarioMP = data.abr;
            sessionStorage.nombreUsuarioMP = data.nombre;
            sessionStorage.emailUsuarioMP = data.email;
            sessionStorage.admin = data.admin;
            sessionStorage.statusMP = 'true';
            hideLoader('#modalSign');
            mostrarSesion();
            if (data.admin === '1'){
              window.location = '/panel.html';
            }else{
              window.location = '/cotizar.html';
            }
          }
        }else{
          errorLoader('#modalSign');
          $('#errorLoginFoo').addClass('alert alert-danger').html('<i class="fa fa-exclamation-triangle"> Invalida combinación email/contraseña');
          $('input[type="password"]').val('');
          $('#modalSign').shake(3,15,800);
        }
      });
    }
});
