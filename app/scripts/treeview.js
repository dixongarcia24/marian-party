/* jshint devel:true */
'use strict';

$(function() {
  var defaultData = JSON.parse('[{"text":"Espectáculos ","id":"1", "nodes": [{"text":"Transformers","id":"1"}, {"text":"Frozen","id":"2"}, {"text":"Princesas de Disney","id":"3"}, {"text":"Los Vengadores ","id":"4"}, {"text":"Sirenita","id":"5"}, {"text":"Malvados de Disney","id":"6"}, {"text":"Spiderman","id":"8"}, {"text":"Peppa Pig y sus Amigas","id":"14"}, {"text":"Violetta","id":"24"}, {"text":"Batman","id":"25"}, {"text":"Sofia","id":"26"}, {"text":"Rapunzell","id":"27"}, {"text":"Blanca Nieves","id":"29"}]}, {"text":"Animaciones Infantiles","id":"2", "nodes": [{"text":"Recreación","id":"11"}, {"text":"Bailando por el Mundo","id":"12"}]}, {"text":"Animaciones para Adultos","id":"3", "nodes": [{"text":"Show Halloween","id":"7"}, {"text":"Maquillaje Artistico","id":"10"}]}, {"text":"Animaciones Corporativas","id":"4", "nodes": []}, {"text":"Otros Servicios","id":"5", "nodes": [{"text":"Inflables","id":"9"}, {"text":"Carro de Frappe","id":"15"}, {"text":"Carro de Hamburguesas","id":"16"}, {"text":"Carro de Shawarma","id":"17"}, {"text":"Pasapalos","id":"18"}, {"text":"Pinchos","id":"19"}, {"text":"Sonido con Luces","id":"20"}, {"text":"Maquina de Burbujas","id":"21"}, {"text":"Decoración con Globos y Telas","id":"22"}, {"text":"Piñateria","id":"23"}, {"text":"Spa Chiquiticas","id":"30"}]}, {"text":"Dizfrácez","id":"6", "nodes": []}]');
  var $checkableTree = $('#treeview-checkable').treeview({
    data: defaultData, // jshint ignore:line
    levels:99,
    showIcon: false,
    showCheckbox: true,
    showBorder: false,
    highlightSelected: false,
    onNodeChecked: function(event, node) {
      noCheckParents();
      $('#checkable-output').append('<li><p id=' + node.id  +'><i class="fa fa-angle-right"></i>' + node.text +'</p></li>');
      btnCotizar();
    },
    onNodeUnchecked: function (event, node) {
      noCheckParents();
      $('#'+node.id).parent('li').remove();
      btnCotizar();

      //$('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
    },
    onNodeCollapsed: function() {
      //noCheckParents();
    },
    onNodeExpanded: function () {
      noCheckParents();
    },
    onNodeSelected: function() {
      noCheckParents();
    },
    onNodeUnselected: function () {
      noCheckParents();
    }
  });

  var findCheckableNodess = function() {

    return $checkableTree.treeview('search', [ $('#input-check-node').val(), { ignoreCase: false, exactMatch: false } ]);
  };
  var checkableNodes = findCheckableNodess();

  // Check/uncheck/toggle nodes
  $('#input-check-node').on('keyup', function () {
    checkableNodes = findCheckableNodess();
    $('.check-node').prop('disabled', (checkableNodes.length < 1));
  });

  $('#btn-check-node.check-node').on('click', function () {
    $checkableTree.treeview('checkNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
  });

  $('#btn-uncheck-node.check-node').on('click', function () {
    $checkableTree.treeview('uncheckNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
  });

  $('#btn-toggle-checked.check-node').on('click', function () {
    $checkableTree.treeview('toggleNodeChecked', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
  });

  // Check/uncheck all
  $('#btn-check-all').on('click', function () {
    $checkableTree.treeview('checkAll', { silent: $('#chk-check-silent').is(':checked') });
  });

  $('#btn-uncheck-all').on('click', function () {
    $checkableTree.treeview('uncheckAll', { silent: $('#chk-check-silent').is(':checked') });
  });

  function btnCotizar() {
    if ($('#checkable-output').children().length === 0){
        $('#btn-cotizar').empty();
      }
      else{
          if ($('#btn-cotizar').is(':empty')) {
            $('#btn-cotizar').append('<button id="cotizando" class="btn btn-primary" data-toggle="modal" data-target="#modalCotizar"> Cotizar</button>');
          }
      }

  }

  function noCheckParents(){
    $('#treeview-checkable').click(function () {
      $('.node-treeview-checkable:has(.fa-chevron-down):has(.fa-square-o)').find('.fa-square-o').remove();
      $('.node-treeview-checkable:has(.fa-chevron-right):has(.fa-square-o)').find('.fa-square-o').remove();
    });
  }

  $(document).ready(function() {
    $('.node-treeview-checkable:has(.fa-chevron-down)' ).click ();
    $('.node-treeview-checkable:has(.fa-chevron-down)' ).click ();
    $('.node-treeview-checkable:has(.fa-chevron-down):has(.fa-square-o)').find('.fa-square-o').remove();
  });


});

