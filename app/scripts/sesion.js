'use strict';

function session(){
  var user = null;
  if(typeof(Storage) !== 'undefined'){
    if (sessionStorage.getItem('logged')) {
      if (sessionStorage.getItem('logged') === 'true'){
        user = JSON.parse(sessionStorage.getItem('user'));
      }
    }
  }
  return user;
}

$(document).ready(function() {
  if(session() !== 'true'){
    //Modal Prueba
    $('#myModal').modal('show');

    /*
    //Modal Final
    $('#myModal').modal({show:true, backdrop: 'static', keyboard: false});
    */
  }
});
