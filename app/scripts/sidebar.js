'use strict';
$(document).ready(function () {
  $('[data-toggle="offcanvas"]').click(function () {
    $('.row-offcanvas').toggleClass('active');
  });
});

$(document).ready(function ($) {
  $('#tabs').tab();
  });

$('.list-group-item').click(function(e) {
    e.preventDefault();
    $('.list-group-item').removeClass('active');
    $(this).addClass('active');
});

$('.toggleBtn').click(function(e) {
  e.preventDefault();
  if ($(document.body).attr('class') === 'scroll'){
    setTimeout(function() {
      $('html').removeClass('scroll');
      $('body').removeClass('scroll');
    }, 800);
  }
  else{
    $('html').addClass('scroll');
    $('body').addClass('scroll');
  }
});

$('#tabAll').click(function(){
  $('.tab-pane').each(function(i,t){ // jshint ignore:line
    $('#tabs a').removeClass('active');
    $(this).addClass('active');
    $(this).addClass('in');
  });
});
$(document).ready(function() {
  $('#bToggle').click(function() {
    if($('#bToggle').hasClass('fa-chevron-left')){
      $('#bToggle').removeClass('fa-chevron-left').addClass('fa-chevron-right');
    }
    else{
      $('#bToggle').removeClass('fa-chevron-right').addClass('fa-chevron-left');

    }
  });
});
