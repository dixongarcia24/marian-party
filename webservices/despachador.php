<?php
header('Content-type: text/html;charset=utf-8');
header('Access-Control-Allow-Origin: *');
require_once("libDespachador.php");
class Despachador {

 function despachar()
 {
  $servicio = $_REQUEST['servicio'];
  $objServicio = new Servicios();
  switch ($servicio) {
    case 0:
      $contenido = $objServicio->servicio00();
      break;
    case 1:
      //Servicio iniciar sesión
      $email = $_REQUEST['email'];
      $clave = $_REQUEST['clave'];
      $contenido = $objServicio->servicio01($email,$clave);
      break;
    case 2:
      //Servicio Registrar Cliente
      $abr = $_REQUEST['abr'];
      $nombre = $_REQUEST['nombre'];
      $email = $_REQUEST['email'];
      $tel = $_REQUEST['telefono'];
      $emp = $_REQUEST['empresa'];
      $ciu = $_REQUEST['ciudad'];
      $cla = $_REQUEST['clave'];
      $contenido = $objServicio->servicio02($abr,$nombre,$email,$tel,$emp,$ciu,$cla);
      break;
    case 3:
      //Servicio verificar captcha
      $response = $_REQUEST['response'];
      $contenido = $objServicio->Servicio03($response);
      break;
    case 4:
      //Servicio retorna estados
      $contenido = $objServicio->servicio04();
      break;
    case 5:
      //Servicio buscar todas las ciudades de un estado
      $estado = $_REQUEST['estado'];
      $contenido = $objServicio->Servicio05($estado);
      break;
    case 6:
      //Servicio verificación cuenta
      $usuario = $_REQUEST['idusuario'];
      $token = $_REQUEST['token'];
      $contenido = $objServicio->Servicio06($usuario,$token);
      break;
    case 7:
      //Servicio enlace restablecer contraseña
      $email = $_REQUEST['email'];
      $contenido = $objServicio->Servicio07($email);
      break;
    case 8:
      //Servicio restablecer contraseña
      $usuario = $_REQUEST['usuario'];
      $token= $_REQUEST['token'];
      $password= $_REQUEST['password'];
      $contenido = $objServicio->Servicio08($usuario,$token,$password);
      break;
    case 9:
      //Servicio retorna suscriptores
      $contenido = $objServicio->Servicio09();
      break;
    case 10:
      //Servicio retorna servicios
      $contenido = $objServicio->Servicio10();
      break;
    case 11:
      //Servicio retorna precio de los servicios
      $id= $_REQUEST['id'];
      $contenido = $objServicio->Servicio11($id);
      break;
    case 12:
      //Servicio retorna imagenes de los servicios
      $id = $_REQUEST['id'];
      $contenido = $objServicio->Servicio12($id);
      break;
    case 13:
      //Servicio que retorna json de los servicios para el treeview
      $contenido = $objServicio->Servicio13();
      break;
    case 14:
      //Servicio para cotizar
      $id = $_REQUEST['id'];
      $fecha = $_REQUEST['fecha'];
      $seleccion = json_decode($_REQUEST['seleccion']);
      $ciudad = $_REQUEST['ciudad'];
      $direccion = $_REQUEST['direccion'];
      $contenido = $objServicio->Servicio14($id,$fecha,$seleccion,$ciudad,$direccion);
      break;
    case 15:
      //Servicio actualiza precio servicio
      $id = $_REQUEST['id'];
      $precio = $_REQUEST['precio'];
      $contenido = $objServicio->Servicio15($id,$precio);
      break;
    case 16:
      $id = $_REQUEST['id'];
      $contenido = $objServicio->Servicio16($id);
      break;
    case 17:
      //Servicio actualiza precio de transporte
      $id = $_REQUEST['id'];
      $precio = $_REQUEST['precio'];
      $contenido = $objServicio->Servicio17($id,$precio);
      break;
    case 86:
      //servicio para guardar una publicacion
      $Fecha = $_REQUEST['fechaActual'];
      $Descripcion = $_REQUEST['descripcion'];
      $Estatus = $_REQUEST['estatus'];
      $Tiponoticia = $_REQUEST['tiponoticia'];
      $Idlogin = $_REQUEST['idlogin'];
      $Cedula = $_REQUEST['cedula'];
      $Idcondominio = $_REQUEST['idcondominio'];
      $contenido = $objServicio->Servicio086($Fecha,$Descripcion,$Estatus,$Tiponoticia,$Idlogin,$Cedula,$Idcondominio);
      break;
    case 87:
      //Servicio que retorna los tipos de noticia
      $contenido = $objServicio->Servicio087();
      break;
    case 88;
      //Servicio que registra un reclamo/sugerencia
      $razon = $_REQUEST['razonreclamosugerencia'];
      $fecha = $_REQUEST['fechareclamosugerencia'];
      $descripcion = $_REQUEST['descripcionreclamosugerencia'];
      $estatus = 'A';
      $idinmueble = $_REQUEST['idinmueblereclamosugerencia'];
      $contenido = $objServicio->Servicio088($razon,$fecha,$descripcion,$estatus,$idinmueble);
      break;

  }
  echo $contenido;
  return true;
 }
}
$objDespachador = new Despachador();
$objDespachador->despachar();

?>
