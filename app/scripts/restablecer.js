/* jshint devel:true */
/*global alertas*/
'use strict';
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function() {
  var usuario = getUrlParameter('idusuario');
  var token = getUrlParameter('token');
  var contenido = '';
  if(usuario){
    contenido = '<form id="form-cambio" role="form" class="form-horizontal">'+
                  '<div class="form-group">'+
                    '<div class="col-sm-12">'+
                      '<h3 id="mensaje">Ingrese su nueva contraseña</h3>'+
                    '</div>'+
                  '</div>'+
                  '<div class="form-group">'+
                    '<label for="password" class="col-sm-2 control-label">Contraseña</label>'+
                    '<div class="col-sm-10">'+
                      '<input id="password" type="password" placeholder="Introduzca su nueva contraseña" class="form-control"/>'+
                    '</div>'+
                  '</div>'+
                  '<div class="form-group">'+
                    '<label for="confirm" class="col-sm-2 control-label">Confirmar contraseña</label>'+
                    '<div class="col-sm-10">'+
                      '<input id="confirm" type="password" placeholder="Confirme su nueva contraseña" class="form-control"/>'+
                    '</div>'+
                  '</div>'+
                  '<div class="row">'+
                    '<div class="col-sm-2"></div>'+
                    '<div class="col-sm-10">'+
                      '<button id="cambiar" type="button" class="btn btn-primary btn-sm">Cambiar Contraseña</button>'+
                    '</div>'+
                  '</div>'+
                '</form>';
    $('#restableciendo').empty();
    $('#restableciendo').append(contenido);
  }else{
    contenido = '<form id="form-link" role="form" class="form-horizontal">'+
                      '<div class="form-group">'+
                        '<div class="col-sm-12">'+
                          '<h3 id="mensaje">Introduzca su correo para restablecer su contraseña</h3>'+
                        '</div>'+
                      '</div>'+
                      '<div class="form-group">'+
                        '<label for="email" class="col-sm-2 control-label">Correo</label>'+
                        '<div class="col-sm-10">'+
                          '<input id="email" type="email" placeholder="¿Cuál es su email?" class="form-control"/>'+
                        '</div>'+
                      '</div>'+
                      '<div class="row">'+
                        '<div class="col-sm-2"></div>'+
                        '<div class="col-sm-10">'+
                          '<button id="enviar" type="button" class="btn btn-primary btn-sm">Restablecer Contraseña</button>'+
                        '</div>'+
                      '</div>'+
                    '</form>';
    $('#restableciendo').empty();
    $('#restableciendo').append(contenido);
  }

  $('#enviar').click(function(){
    $('#loading').show();
    var email = $('#email').val();
    if(email===''){
      alertas('alert-danger','Debe ingresar su correo electrónico.','fa-exclamation-triangle');
      $('#email').focus();
    }else{
      var url = '//beta.marianparty.com/MPWebService/despachador.php?servicio=7&email='+email;
      $.getJSON(url,function(data){
        if(data.existe==='true'){
          $('#loading').hide();
          alertas('alert-success','Se le envió un correo para restablecer su contraseña','fa-check');
          $('#form-link').empty();
        }else if (data.existe==='no-user') {
          $('#loading').hide();
          alertas('alert-danger','El correo suministrado no pertenece a ningún usuario registrado, por favor verifique.','fa-exclamation-triangle');
        }else{
          $('#loading').hide();
          alertas('alert-danger','Ha ocurrido un error, por favor intente más tarde.','fa-exclamation-triangle');
        }
      });
    }
  });

  $('#cambiar').click(function(){
    $('#loading').show();
    var pass = $('#password').val();
    var conf = $('#confirm').val();
    if(pass==='' || conf===''){
      alertas('alert-danger','Debe rellenar los campos.','fa-exclamation-triangle');
    }else if (pass!==conf) {
      alertas('alert-danger','No coinciden los campos.','fa-exclamation-triangle');
      $('#confirm').focus();
    }else{
      var url = '//beta.marianparty.com/MPWebService/despachador.php?servicio=8&usuario='+usuario+'&token='+token+'&password='+pass;
      $.getJSON(url,function(data){
        if (data.existe==='true') {
          $('#loading').hide();
          $('#form-cambio').empty();
          alertas('alert-success','Su contraseña ha sido actualizada exitosamente, se redireccionará al inicio en 5 segundos.','fa-check');
          setTimeout(function(){
            window.location = '/';
          },5000);
        }else if (data.existe==='noCambio'){
          $('#loading').hide();
          $('#form-cambio').empty();
          alertas('alert-danger','Ocurrió un problema al cambiar la contraseña, intente más tarde.','fa-check');
          setTimeout(function(){
            window.location = '/';
          },5000);
        }else{
          $('#form-cambio').empty();
          $('#loading').hide();
          alertas('alert-danger','El enlace fue usado o ha caducado. Solicite otro cambio de contraseña.','fa-check');
          setTimeout(function(){
            window.location = '/';
          },5000);
        }
      });
    }
  });

});
